



~~~java

## 抽象类题目4

创建类描述黄瓜、茄子、香蕉、榴莲；它们各自拥有的属性和功能如下：（后面带小括号的是功能）

黄瓜：颜色；重量；可以吃(){黄瓜炒蛋}；可以使用(){做面膜}

茄子：颜色；重量；可以吃(){油焖茄子}；可以使用(){做中药}

香蕉：颜色；重量；可以吃(){脆皮香蕉}；可以使用(){做香蕉面膜}

榴莲：颜色；重量；可以吃(){榴莲酥}；可以使用(){砸人}

请用继承的思想设计设计以上类型。

答案：

```java

```





public abstract  class Vegetable {

    private String color;
    private double weight;

    public abstract  void eat();
    public abstract  void use();

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Vegetable(String color, double weight) {
        this.color = color;
        this.weight = weight;
    }

    public Vegetable() {
    }



}



public class Cuke extends Vegetable {
    @Override
    public void eat() {
        System.out.println("可以吃黄瓜炒蛋");
    }

    @Override
    public void use() {
        System.out.println("可以做面膜");
    }

}


public class Eggplant extends Vegetable {
    @Override
    public void eat() {
        System.out.println("可以吃油焖茄子");
    }

    @Override
    public void use() {
        System.out.println("可以做中药");
    }

}




public class Banana extends Vegetable{
    @Override
    public void eat() {
        System.out.println("可以吃脆皮香蕉");
    }

    @Override
    public void use() {
        System.out.println("可以做香蕉面膜");
    }
}


public class Durian extends Vegetable{
    @Override
    public void eat() {
        System.out.println("可以吃榴莲酥");
    }

    @Override
    public void use() {
        System.out.println("可以砸人");
    }


}




public class Test {
    public static void main(String[] args) {
    Cuke cu = new Cuke();
    cu.setColor("黄色");
    cu.setWeight(220);
    System.out.println("黄瓜颜色：" + cu.getColor() + " 黄瓜重量：" + cu.getWeight() + "克");
    cu.eat();
    cu.use();
        Eggplant au = new Eggplant();
    au.setColor("紫色");
    au.setWeight(200);
    System.out.println("茄子颜色：" + au.getColor() + " 茄子重量：" + au.getWeight() + "克");
    au.eat();
    au.use();
    Banana ba = new Banana();
    ba.setColor("黄色");
    ba.setWeight(120);
    System.out.println("香蕉颜色：" + ba.getColor() + " 香蕉重量：" + ba.getWeight() + "克");
    ba.eat();
    ba.use();
    Durian du = new Durian();
    du.setColor("青黄色");
    du.setWeight(500);
    System.out.println("榴莲颜色：" + cu.getColor() + " 榴莲重量：" + cu.getWeight() + "克");
    du.eat();
    du.use();
}
}

~~~

