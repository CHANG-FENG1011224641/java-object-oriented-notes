## 题目3

将以下描述信息转换为java代码：

1、定义员工Employee类,该类具有如下成员:

```java
(1) 属性：姓名(name,字符串类型)，工号(workId,字符串类型)，部门(dept,字符串类型),属性私有
(2) 方法:
		1. 空参数构造和满参数构造
		2. 提供属性的set/get方法
		3. 定义showMsg方法抽象方法
```



2、定义经理Manager类继承Employee类，该类具有如下成员:

```
(1) 属性:职员Clerk(该经理的职员)
(2) 方法:
		1. 空参数构造方法和满参数构造方法
		2. 属性的get和set方法
		3. 重写父类的showMsg方法，按照要求实现信息打印
```



3、定义职员Clerk类继承Employee类，该类具有如下成员:

```
(1) 属性:经理Manager(该职员的经理)
(2) 方法:
		1. 空参数构造方法和满参数构造方法
		2. 属性的get和set方法
		3. 重写父类的showMsg方法，按照要求实现信息打印
```



4、创建Test测试类，测试类中创建main方法，main方法中创建经理对象和职员对象，信息分别如下:  

```java
经理：工号为 M001,名字为 张小强，部门为 销售部
职员：工号为 C001,名字为 李小亮，部门为 销售部 

经理的职员为李小亮，职员的经理为张小强
```

分别调用经理的showMsg方法和职员的showMsg方法打印结果：

```java
销售部的：张小强，员工编号：M001
他的职员是李小亮
销售部的：李小亮，员工编号：C001
他的经理是张小强
```





# 答案

```java
public class Clerk extends Employee{
    private String Manager_name;

    public String getManager_name() {
        return Manager_name;
    }

    public void setManager_name(String manager_name) {
        Manager_name = manager_name;
    }

    public Clerk(String name, String workId, String dept, String manager_name) {
        super(name, workId, dept);
        Manager_name = manager_name;
    }

    public Clerk(String manager_name) {
        Manager_name = manager_name;
    }

    public Clerk(String name, String workId, String dept) {
        super(name, workId, dept);
    }

    public Clerk() {
    }

    @Override
    public void showMsg() {
        System.out.println(getDept()+"的:"+getName()+",员工编号:"+getWorkId());
        System.out.println("他的职员是"+getManager_name());

    }
}


public abstract class Employee {
    private String name;
    private String workId;
    private String dept;

   public abstract void showMsg(
    );

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWorkId() {
        return workId;
    }

    public void setWorkId(String workId) {
        this.workId = workId;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public Employee(String name, String workId, String dept) {
        this.name = name;
        this.workId = workId;
        this.dept = dept;
    }

    public Employee() {
    }
}


public class Manager extends Employee {
    private String Clerk_name;

    public String getClerk_name() {
        return Clerk_name;
    }

    public void setClerk_name(String clerk_name) {
        Clerk_name = clerk_name;
    }

    public Manager(String name, String workId, String dept, String clerk_name) {
        super(name, workId, dept);
        Clerk_name = clerk_name;
    }

    public Manager(String clerk_name) {
        Clerk_name = clerk_name;
    }

    public Manager(String name, String workId, String dept) {
        super(name, workId, dept);
    }

    public Manager() {
    }

    @Override
    public void showMsg() {
        System.out.println(getDept()+"的:"+getName()+",员工编号:"+getWorkId());
        System.out.println("他的职员是"+getClerk_name());
    }
}



public class Test {
    public static void main(String[] args) {
        Manager M = new Manager("M001","张小强","销售部","李小亮");
        M.showMsg();

        Clerk C = new Clerk("C001","李小亮","销售部","张小强");
        C.showMsg();

    }


}




```


